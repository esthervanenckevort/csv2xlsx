/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.allthingsdigital.csv2xlsx;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author david
 */
public class Application {
    public static void main(final String[] args) throws IOException {
        if (args.length >= 2) {
            final Path csv = Paths.get(args[0]);
            final Path xlsx = Paths.get(args[1]);
            final String sheetName = args.length == 3 ? args[2] : csv.toFile().getName();
            try (Workbook workbook = new XSSFWorkbook()) {
                final Sheet sheet = workbook.createSheet(sheetName);
                
                CSVParser.parse(csv.toFile(), StandardCharsets.UTF_8, CSVFormat.DEFAULT).iterator().forEachRemaining(record -> {
                    Row row;
                    if (!sheet.rowIterator().hasNext()) {
                        row = sheet.createRow(0);
                    } else {
                        row = sheet.createRow(sheet.getLastRowNum() + 1);
                    }
                    record.forEach(column -> {
                        int cellNum = row.getLastCellNum();
                        final Cell cell = row.createCell(cellNum == -1 ? 0 : cellNum);
                        cell.setCellValue(column);
                    });
                });
                workbook.write(Files.newOutputStream(xlsx));
            }
        } else {
            System.err.println("At least 2 parameters required.\nUsage: input.csv output.xsls optional_sheet_name");
        }
    }
}
